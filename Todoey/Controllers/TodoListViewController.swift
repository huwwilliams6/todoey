//
//  ViewController.swift
//  Todoey
//
//  Created by Huw Williams on 27/08/2019.
//  Copyright © 2019 The bubberkins. All rights reserved.
//
//Only use UserDefaults for storing very small amounts of data - do not use it as a database!
import UIKit

class TodoListViewController: UITableViewController {

    //var rather than let here allows for the array to be changed at later points
    var itemArray = [Item]()
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let newItem = Item()
        newItem.title = "Buy Eggs"
        itemArray.append(newItem)
        
        let newItem2 = Item()
        newItem2.title = "Buy Oats"
        itemArray.append(newItem2)
        
        let newItem3 = Item()
        newItem3.title = "Buy Milk"
        itemArray.append(newItem3)
        //Code to retrieve the items from the userdefauls and assign them to the itemArray in order to persist/display them
        if let items = defaults.array(forKey: "TodoListArray") as? [Item] {
           itemArray = items
        }
        // Do any additional setup after loading the view.
    }

    //Mark - Tableview Datasource Methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ToDoItemCell", for: indexPath)
        
        let item = itemArray[indexPath.row]
        
        cell.textLabel?.text = item.title
        
        //ternary operator - cuts down code
        //value = condition ? valueIfTrue : valueIfFalse
        //essentially saying to set the cells accessory type depending on whather the item.done is true. if it true then set it to .checkmark and if its not true, set it to .none

        cell.accessoryType = item.done ? .checkmark : .none
        
        
        return cell
    }
    
    //MARK - TableView delegate methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print(itemArray[indexPath.row])
        
        //setting the item property 'done' to the opposite of what is is when the cell is clicked
        itemArray[indexPath.row].done = !itemArray[indexPath.row].done
        //Code to check and uncheck each list item
        
        tableView.reloadData()
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
    
    //MARK - Add new items to list

    @IBAction func AddBtnPressed(_ sender: UIBarButtonItem) {
        
        // creating a local variable for the text field so that it is accessible outside of the closure of just the addTextFIeld {}
        var textField = UITextField()
        
        let alert = UIAlertController(title: "Add New Item", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add Item", style: .default) {(action) in
           
            let newItem = Item()
            newItem.title = textField.text!
            //statement below = what happens once you press the add item button
            self.itemArray.append(newItem)
            //once you have added an item to the array you still need to reload the table to update it - the method for this is tableview.reloadData().
            //save updated item Array to user defaults - always have to add self. when you are in a closure
            //always need a Key for saving to userdefaults as it operates as a dictionary. It can take any value - an array, dictionary etc.
            self.defaults.set(self.itemArray, forKey: "TodoListArray")
            self.tableView.reloadData()
        }
        
        //Adding a text field to the alert
        
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Create new item"
            textField = alertTextField
        }
    
    alert.addAction(action)
    
    present(alert,animated:true, completion: nil)
    
    }
}

