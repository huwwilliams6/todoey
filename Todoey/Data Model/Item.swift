//
//  Item.swift
//  Todoey
//
//  Created by Huw Williams on 17/09/2019.
//  Copyright © 2019 The bubberkins. All rights reserved.
//

import Foundation

class Item {
    
    var title: String = ""
    var done: Bool = false
    
}
